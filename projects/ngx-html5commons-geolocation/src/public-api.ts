/*
 * Public API Surface of ngx-html5commons-geolocation
 */

export * from './lib/services/commons-html5-geolocation.service';

export * from './lib/ngx-html5commons-geolocation.module';
