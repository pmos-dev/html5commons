import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommonsHtml5GeolocationService } from './services/commons-html5-geolocation.service';

@NgModule({
		imports: [
				CommonModule
		],
		declarations: []
})
export class NgxHtml5CommonsGeolocationModule {
	static forRoot(): ModuleWithProviders {
		return {
				ngModule: NgxHtml5CommonsGeolocationModule,
				providers: [
						CommonsHtml5GeolocationService
				]
		};
	}
}
