import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

import { ICommonsGeographic } from 'tscommons-geographics';

@Injectable()
export class CommonsHtml5GeolocationService {
	public static fetch(highAccuracy: boolean = false): Promise<ICommonsGeographic|undefined> {
		const lamdba: Promise<ICommonsGeographic|undefined> = new Promise((resolve: (_: ICommonsGeographic|undefined) => void, _): void => {
			if (!navigator.geolocation) {
				console.log('HTML5 geo not supported');
				resolve(undefined);
				return;
			}
				
			navigator.geolocation.getCurrentPosition(
					(position: Position): void => {
						const detected: ICommonsGeographic = {
								latitude: position.coords.latitude,
								longitude: position.coords.longitude,
								radius: position.coords.accuracy
						};
						console.log('HTML5 geo detected');
						resolve(detected);
						return;
					},
					(error: unknown): void => {
						console.log(error);
						resolve(undefined);
						return;
					},
					{
							enableHighAccuracy: highAccuracy,
							maximumAge: Infinity,
							timeout: 15000
					}
			);
		});
		
		return lamdba;
	}
	
	private onLocationChanged: EventEmitter<ICommonsGeographic|undefined> = new EventEmitter<ICommonsGeographic|undefined>(true);
	
	private last: ICommonsGeographic|undefined;
	
	public setLocation(geographic: ICommonsGeographic|undefined) {
		this.last = geographic;
		this.onLocationChanged.emit(geographic);
	}
	
	public getLastLocation(): ICommonsGeographic|undefined {
		return this.last;
	}
	
	public locationChangedObservable(): Observable<ICommonsGeographic|undefined> {
		return this.onLocationChanged;
	}

	public detect(useLastOnFail: boolean = false): boolean {
		if (!navigator.geolocation) {
			console.log('HTML5 geo not supported');
			if (useLastOnFail) this.onLocationChanged.emit(this.last);
			return false;
		}
		
		const lambda: () => Promise<void> = async (): Promise<void> => {
			const location: ICommonsGeographic|undefined = await CommonsHtml5GeolocationService.fetch();

			if (location !== undefined) {
				this.setLocation(location);
				this.onLocationChanged.emit(location);
			} else {
				if (useLastOnFail && this.last !== undefined) {
					this.onLocationChanged.emit(this.last);
				}
			}
		};
		lambda();
		
		return true;
	}
}
